
# prex-ui

# pre-ui

#### 介绍

**Perx**基于Spring Boot 2.1.8  、Spring Cloud Greenwich.SR3、Spring Alibaba Cloud、Spring Security 、Spring cloud Oauth2 、Vue的前后端分离的的RBAC权限管理系统，项目支持数据权限管理，支持后端配置菜单动态路由,第三方社交登录, 努力做最简洁的后台管理系统

**Prex** 前端源码

#### 项目源码

|     |   后端源码  |   前端源码  |
|---  |--- | --- |
|  码云   |  https://gitee.com/kaiyuantuandui/prex   |  https://gitee.com/kaiyuantuandui/prex-ui   |


#### 前端模板

初始模板基于： https://vue.ant.design/


#### 安装教程

``` bash
# 安装依赖
npm install

# 启动服务 localhost:8000
npm run serve

# 构建生产环境
npm run build
```

